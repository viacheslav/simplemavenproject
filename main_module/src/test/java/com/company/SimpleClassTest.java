package com.company;

import org.junit.Assert;
import org.junit.Test;

public class SimpleClassTest {

    private SimpleClass simpleClass = new SimpleClass();

    @Test
    public void testPlus() {
        int result = simpleClass.plus(1, 2);
        Assert.assertEquals(3, result);
    }
}